package com.example.modifycalculator

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.EditText
import android.widget.TextView
import kotlinx.android.synthetic.main.activity_main.*
import java.lang.Float.parseFloat
import java.util.*

class MainActivity : AppCompatActivity() {

    lateinit var num1: TextView
    lateinit var display: TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        initView()
        //setOnClick()
    }

    private fun initView() {
        num1 = findViewById(R.id.num1_myCal)
        display = findViewById(R.id.display_myCal)
    }

    fun buttonClickHandler(view: View) {
        when (view.id) {
            R.id.btn0 -> num1.append("0")
            R.id.btn1 -> num1.append("1")
            R.id.btn2 -> num1.append("2")
            R.id.btn3 -> num1.append("3")
            R.id.btn4 -> num1.append("4")
            R.id.btn5 -> num1.append("5")
            R.id.btn6 -> num1.append("6")
            R.id.btn7 -> num1.append("7")
            R.id.btn8 -> num1.append("8")
            R.id.btn9 -> num1.append("9")

            R.id.btnDiv -> num1.append("/")
            R.id.btnMulti -> num1.append("*")
            R.id.btnMinus -> num1.append("-")
            R.id.btnPlus -> num1.append("+")

            R.id.btnDel -> if (num1.text.toString().isNotEmpty()) {
                val strTextTmp =
                    num1.text.toString().substring(0, num1.text.toString().length - 1)

                num1.text = ""
                num1.append(strTextTmp)
            }
            R.id.btnClear -> {
                num1.text = ""
                display.text = ""
            }
            R.id.btnTotal -> {
                // เริ่มคำนวน
                val strStack = infixToPostfix(num1.text.toString())
                val intValue = Calculator(strStack)

                // นำค่าที่คำนวนได้กลับไปใส่ใน text2
                display.text = intValue.toString()
            }
        }
    }

    // ตรวจสอบลำดับของเครื่องหมาย * / + -
    fun getOperator(chaOperator: Char): Int {
        if (chaOperator == '+' || chaOperator == '-') {
            return 1
        } else if (chaOperator == '*' || chaOperator == '/') {
            return 2
        }
        return 0
    }

    // ตรวจสอบว่าเป็นตัวเลขหรือไม่
    fun isFloat(strInput: String): Boolean {
        try {
            parseFloat(strInput)
            return true
        } catch (e: Exception) {
            return false
        }
    }

    //แปลงจาก infix เป็น postfix
    fun infixToPostfix(strInfix: String): String {
        var strInfix = strInfix
        var strExpression: String
        var strPostfix = " "

        //toRegex = แปลง string เป็นนิพจน์
        strInfix = strInfix.replace("[+()\\-*/]".toRegex(), " $0 ")
        Log.d("Log strInfix >>", strInfix)

        val strToken = StringTokenizer(strInfix)

        val operatorStack = Stack<Char>()

        while (strToken.hasMoreTokens()) {
            strExpression = strToken.nextToken()

            when {
                Character.isDigit(strExpression[0]) -> {

                    strPostfix = strPostfix + " " + parseFloat(strExpression)
                    //Log.d("Log strPostfix >>", strPostfix)
                }
                /*strExpression == "(" -> {
                    val operator = '('
                    operatorStack.push(operator)
                }
                strExpression == ")" -> {
                    while ((operatorStack.peek() as Char).toChar() != '(') {
                        strPostfix = strPostfix + " " + operatorStack.pop()
                    }
                    operatorStack.pop()
                }*/
                else -> {
                        //peep การนำข้อมูลมาใช้แต่ไม่ลบออก
                    while (!operatorStack.isEmpty() && getOperator(strExpression[0]) <= getOperator((operatorStack.peek() as Char).toChar())) {
                        strPostfix = strPostfix + " " + operatorStack.pop()
                        Log.d("Log OperatorStack", "$operatorStack")
                        Log.d("Log strPostfix >>", strPostfix)
                        //Log.d("Log strExpression[] >>", strExpression)
                    }

                    val operator = strExpression[0]
                    operatorStack.push(operator)
                    Log.d("Log operator >>", "$operator")
                }
            }
        }

        while (!operatorStack.isEmpty()) {
            strPostfix = strPostfix + " " + operatorStack.pop()
        }
        return strPostfix
    }

    // ทำการคำนวน  + - * / จาก postfix
    fun Calculator(strPostfix: String): Float {
        var a: Float
        var b: Float
        var result = 0f

        val arrPostfix =
            strPostfix.split(" ".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
        //Log.d("strPostfix >>", "$arrPostfix")

        val CalStack = Stack<Float>()

        for (i in arrPostfix.indices) {
            val ch = arrPostfix[i]
           // Log.d("Log ch >>", ch)

            if (isFloat(ch)) {
                CalStack.push(parseFloat(ch))  //push เพิ่มข้อมูลลง stack
                //Log.d("Log calStack >>", "$CalStack")
            } else {

                if (ch == "+") {
                    a = CalStack.pop() //pop นำข้อมูลไปใช้และลบออกจาก stack
                    //Log.d("Log A pop to St >>", "$a")
                    b = CalStack.pop()
                    //Log.d("Log B pop to St >>", "$b")
                    result = a + b
                    //Log.d("Log Result >>", "$result")
                    CalStack.push(result)
                } else if (ch == "-") {
                    a = CalStack.pop()
                    //Log.d("Log A pop to St >>", "$a")
                    b = CalStack.pop()
                    //Log.d("Log B pop to St >>", "$b")
                    result = b - a
                    //Log.d("Log Result >>", "$result")
                    CalStack.push(result)
                } else if (ch == "*") {
                    a = CalStack.pop()
                    //Log.d("Log A pop to St >>", "$a")
                    b = CalStack.pop()
                    //Log.d("Log B pop to St >>", "$b")
                    result = a * b
                    //Log.d("Log Result >>", "$result")
                    CalStack.push(result)
                } else if (ch == "/") {
                    a = CalStack.pop()
                    //Log.d("Log A pop to St >>", "$a")
                    b = CalStack.pop()
                    //Log.d("Log B pop to St >>", "$b")
                    result = b / a
                    //Log.d("Log Result >>", "$result")
                    CalStack.push(result)
                }
            }
        }

        return result
    }

}
